# Index

::: script
pages = sam.search("*", parameters.get("sort") ? parameters.get("sort") : "label:asc")
:::

::: grid has-header is-striped
spans: {mobile: [9, 3]}
-
[Titre](.index?sort=label)
-
[Date](.index?sort=mtime)
-
{{~ it.pages :page:index}}
{{? page.reference.id.indexOf(".") !== 0 }}
[{{= page.label }}]({{= boka.urlize(page.reference) }})
-
{{= boka.formatTime(page.mtime) }}
-
{{?}}
{{~}}
:::
