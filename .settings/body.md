# Skin

title: Izenah.bzh 
home: accueil 
locales: ["fr"]
logo: izenah.jpg@.settings
style: style.css@.settings
toolbar: true 
random-page-mode: edit
templates:
  page: izenah-template
cerbero:
  stromae: ilienor
  signup: true 
necklace: true
tabs:
  max: 1
nav:
  bottom: never
menu:
  - label: Accueil
  - label: Personnes
    reference: personne
  - label: Lieux
    reference: lieu
  - label: Articles
    reference: article
  - label: Livres
    reference: livre
  - label: Tableaux
    reference: tableau
  - label: Index
    reference: .index
