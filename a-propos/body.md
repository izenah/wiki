# À propos

Izenah.bzh est une encyclopédie ouverte sur l'histoire de l'Île-aux-Moines. Ses contenus sont disponibles sont la licence ouverte [Creative Commons CC-BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/fr/) sauf quand une autre licence est expressément mentionnée. Certaines pages incluent des contenus issus de Wikipédia, mention en est faite explicitement, ainsi que de la licence correspondante. Le logiciel utilisé pour ce site est [Babouk](https://babouk.net). L'ensemble des contenus est disponible sous la forme d'un [projet Git](https://framagit.org/izenah/wiki) hébergé par l'association loi 1901 [Framasoft](https://framasoft.org).


