# Izenah.bzh

::: grid is-gapless
spans: {mobile: 4, tablet: 'one-fifth'}
media: yaml
-
label: À propos
link: a-propos
-
image: vue-du-ciel.jpg@.settings
-
label: Personnes
link: personne
-
image: Ile-aux-Moines_-_Eglise.jpg@eglise-saint-michel
-
label: Lieux
link: lieu
-
image: 42-aau.jpg@.settings
-
label: Articles
link: article
-
image: blason-iam-1.jpg@blason
-
label: Livres
link: livre
-
image: 103-armand-joseph-bonamy-le-bois-de-la-chevre.jpg@le-bois-de-la-chevre-armand-joseph-bonamy
-
label: Tableaux
link: tableau
-
image: 42-aax.jpg@cromlech-de-kergonan
-
label: Événements
link: evenement
-
image: izenah-gwen-a-du-3.svg.png@izenah-gwenn-ha-du
-
label: Index
link: .index
:::


    
