[![Description de cette image, également commentée ci-après](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Gilles_Martin-Chauffier_%282018%29.jpg/220px-Gilles_Martin-Chauffier_%282018%29.jpg)](https://fr.wikipedia.org/wiki/Fichier:Gilles_Martin-Chauffier_(2018).jpg)

Gilles Martin-Chauffier (2018)

| Alias | Gilles Hemsay |
| - | - |
| Naissance | [12](https://fr.wikipedia.org/wiki/12_ao%C3%BBt) [août](https://fr.wikipedia.org/wiki/Ao%C3%BBt_1954) [1954](https://fr.wikipedia.org/wiki/1954_en_litt%C3%A9rature) (66 ans) [Neuilly-sur-Seine](https://fr.wikipedia.org/wiki/Neuilly-sur-Seine) |
| Activité principale | [Journaliste](https://fr.wikipedia.org/wiki/Journaliste), [écrivain](https://fr.wikipedia.org/wiki/%C3%89crivain) |
| Distinctions | [prix Jean-Freustié](https://fr.wikipedia.org/wiki/Prix_Jean-Freusti%C3%A9) ([1996](https://fr.wikipedia.org/wiki/1996_en_litt%C3%A9rature)) [Prix Interallié](https://fr.wikipedia.org/wiki/Prix_Interalli%C3%A9) ([1998](https://fr.wikipedia.org/wiki/1998_en_litt%C3%A9rature)) [Prix Renaudot des lycéens](https://fr.wikipedia.org/wiki/Prix_Renaudot#Prix_Renaudot_des_lyc.C3.A9ens) ([2003](https://fr.wikipedia.org/wiki/2003_en_litt%C3%A9rature)) [Prix Renaudot de l'essai](https://fr.wikipedia.org/wiki/Prix_Renaudot#Prix_Renaudot_de_l.27essai) ([2005](https://fr.wikipedia.org/wiki/2005_en_litt%C3%A9rature)) |

| Langue d’écriture | [français](https://fr.wikipedia.org/wiki/Fran%C3%A7ais) |
| - | - |
| [Genres](https://fr.wikipedia.org/wiki/Genre_litt%C3%A9raire) | [roman](https://fr.wikipedia.org/wiki/Roman_(litt%C3%A9rature)), [essai](https://fr.wikipedia.org/wiki/Essai) |

Œuvres principales

- _[Les Corrompus](https://fr.wikipedia.org/wiki/Les_Corrompus)_ ([1998](https://fr.wikipedia.org/wiki/1998_en_litt%C3%A9rature))
- _Silence, on ment_ ([2003](https://fr.wikipedia.org/wiki/2003_en_litt%C3%A9rature))
- _Le Roman de Constantinople_ ([2004](https://fr.wikipedia.org/wiki/2004_en_litt%C3%A9rature))

 [![Consultez la documentation du modèle](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Info_Simple.svg/12px-Info_Simple.svg.png)](https://fr.wikipedia.org/wiki/Mod%C3%A8le:Infobox_%C3%89crivain)

**Gilles Martin-Chauffier** né **Gilles Martin**[\[1\]](#cite_note-1), également connu sous le pseudonyme de **Gilles Hemsay**, né le [12](https://fr.wikipedia.org/wiki/12_ao%C3%BBt) [août](https://fr.wikipedia.org/wiki/Ao%C3%BBt_1954) [1954](https://fr.wikipedia.org/wiki/1954_en_litt%C3%A9rature) à [Neuilly-sur-Seine](https://fr.wikipedia.org/wiki/Neuilly-sur-Seine), est un [journaliste](https://fr.wikipedia.org/wiki/Journaliste) et un [écrivain](https://fr.wikipedia.org/wiki/%C3%89crivain) [français](https://fr.wikipedia.org/wiki/France), primé à plusieurs reprises et notamment lauréat du [prix Interallié](https://fr.wikipedia.org/wiki/Prix_Interalli%C3%A9) en [1998](https://fr.wikipedia.org/wiki/1998_en_litt%C3%A9rature) et du [prix Jean-Freustié](https://fr.wikipedia.org/wiki/Prix_Jean-Freusti%C3%A9) en 1996.



## Biographie

Gilles Martin-Chauffier est issu d'une vieille famille originaire de [Vannes](https://fr.wikipedia.org/wiki/Vannes). Son père, [Jean Martin-Chauffier](/w/index.php?title=Jean_Martin-Chauffier&action=edit&redlink=1 "Jean Martin-Chauffier (page inexistante)") (1922-1987), fut rédacteur en chef du _[Figaro](https://fr.wikipedia.org/wiki/Le_Figaro)_ et son grand-père, [Louis Martin-Chauffier](https://fr.wikipedia.org/wiki/Louis_Martin-Chauffier) (1894-1980), rédacteur en chef du journal clandestin _[Libération](https://fr.wikipedia.org/wiki/Lib%C3%A9ration_(journal,_1941-1964))_ et l'un des fondateurs du [Comité national des écrivains](https://fr.wikipedia.org/wiki/Comit%C3%A9_national_des_%C3%A9crivains) et du Comité national des journalistes, dans la [Résistance](https://fr.wikipedia.org/wiki/R%C3%A9sistance_fran%C3%A7aise).

Il fait ses études primaires et secondaires au [collège Saint-Jean-de-Passy](https://fr.wikipedia.org/wiki/Lyc%C3%A9e_Saint-Jean-de-Passy) jusqu'en 1972. Diplômé de l'[École supérieure de commerce de Paris](https://fr.wikipedia.org/wiki/%C3%89cole_sup%C3%A9rieure_de_commerce_de_Paris), il part pour [New York](https://fr.wikipedia.org/wiki/New_York) en 1980 afin de préparer un [MBA](https://fr.wikipedia.org/wiki/Ma%C3%AEtrise_en_administration_des_affaires) à l'[université Columbia](https://fr.wikipedia.org/wiki/Universit%C3%A9_Columbia). Il ne mène pas jusqu'au bout cette formation et revient en France, car son premier roman, _Pourpre_, retient l'attention de [Simone Gallimard](https://fr.wikipedia.org/wiki/Simone_Gallimard)[\[2\]](#cite_note-2) qui décide de le publier[\[3\]](#cite_note-3). En 1980, il entre comme journaliste à _[Paris Match](https://fr.wikipedia.org/wiki/Paris_Match)_ y gravissant peu à peu les échelons de la rédaction. Depuis plusieurs années, il est le responsable du cahier « Culture » et l'un des rédacteurs en chef de l'hebdomadaire.

Il poursuit parallèlement une œuvre d'écrivain sous son nom ou sous le nom de plume de Gilles Hemsay. Romancier et essayiste, il écrit bon nombre de ses ouvrages dans la maison de famille sur l'[île aux Moines](https://fr.wikipedia.org/wiki/%C3%8Ele_aux_Moines). Très attaché à la Bretagne, il est l'auteur en 2008 d'un livre sur l'[histoire de la Bretagne](https://fr.wikipedia.org/wiki/Histoire_de_la_Bretagne) _Le Roman de la Bretagne_, et en 2015 d'un livre sur le [golfe du Morbihan](https://fr.wikipedia.org/wiki/Golfe_du_Morbihan), _Une mer de famille_[\[4\]](#cite_note-:0-4). Il fait également partie du jury du [Prix Breizh](https://fr.wikipedia.org/wiki/Prix_Breizh)[\[5\]](#cite_note-5).

## Polémiques

Turcophile notoire[\[6\]](#cite_note-6), Gilles Martin-Chauffier affirme être « tombé sous le charme d’Istanbul » dans les années 1986-1987 à l’occasion d’un reportage effectué pour _[Paris Match](https://fr.wikipedia.org/wiki/Paris_Match)_. Cette passion pour la Turquie l'amène à prendre dans plusieurs articles[\[7\]](#cite_note-7) et à travers plusieurs médias[\[8\]](#cite_note-8) des positions ambiguës sur la question du [génocide des Arméniens](https://fr.wikipedia.org/wiki/G%C3%A9nocide_arm%C3%A9nien). Il a été accusé de [négationnisme](https://fr.wikipedia.org/wiki/N%C3%A9gationnisme)[\[9\]](#cite_note-9).

## Œuvre

### Romans

- [1980](https://fr.wikipedia.org/wiki/1980_en_litt%C3%A9rature) : _Pourpre_, [Mercure de France](https://fr.wikipedia.org/wiki/Mercure_de_France)
- [1982](https://fr.wikipedia.org/wiki/1982_en_litt%C3%A9rature) : _Les Canards du Golden Gate_, Mercure de France
- [1987](https://fr.wikipedia.org/wiki/1987_en_litt%C3%A9rature) : _Sans effort apparent_, [éditions Balland](https://fr.wikipedia.org/wiki/%C3%89ditions_Balland)
- [1991](https://fr.wikipedia.org/wiki/1991_en_litt%C3%A9rature) : _L'Ouest_, [éditions de Fallois](https://fr.wikipedia.org/wiki/%C3%89ditions_de_Fallois)
- [1995](https://fr.wikipedia.org/wiki/1995_en_litt%C3%A9rature) : _Une affaire embarrassante_, [éditions Grasset & Fasquelle](https://fr.wikipedia.org/wiki/%C3%89ditions_Grasset_%26_Fasquelle) – [prix Jean-Freustié](https://fr.wikipedia.org/wiki/Prix_Jean-Freusti%C3%A9)[\[10\]](#cite_note-10)
- [1998](https://fr.wikipedia.org/wiki/1998_en_litt%C3%A9rature) : _[Les Corrompus](https://fr.wikipedia.org/wiki/Les_Corrompus)_, éditions Grasset & Fasquelle – [Prix Interallié](https://fr.wikipedia.org/wiki/Prix_Interalli%C3%A9)
- [2001](https://fr.wikipedia.org/wiki/2001_en_litt%C3%A9rature) : _Belle-amie_, éditions Grasset & Fasquelle
- [2003](https://fr.wikipedia.org/wiki/2003_en_litt%C3%A9rature) : _Silence, on ment_, Grasset & Fasquelle – [Prix Renaudot des lycéens](https://fr.wikipedia.org/wiki/Prix_Renaudot#Prix_Renaudot_des_lycéens)
- [2007](https://fr.wikipedia.org/wiki/2007_en_litt%C3%A9rature) : _Une vraie Parisienne_, éditions Grasset & Fasquelle
- [2011](https://fr.wikipedia.org/wiki/2011_en_litt%C3%A9rature) : _Paris en temps de paix_, éditions Grasset & Fasquelle
- [2014](https://fr.wikipedia.org/wiki/2014_en_litt%C3%A9rature) : _La Femme qui dit non_, éditions Grasset & Fasquelle
- [2018](https://fr.wikipedia.org/wiki/2018_en_litt%C3%A9rature) : _L’Ère des suspects_, éditions Grasset & Fasquelle

### Essais

- [2004](https://fr.wikipedia.org/wiki/2004_en_litt%C3%A9rature) : _Le Roman de Constantinople_, éditions du Rocher – [Prix Renaudot de l'essai](https://fr.wikipedia.org/wiki/Prix_Renaudot#Prix_Renaudot_de_l'essai) [2005](https://fr.wikipedia.org/wiki/Prix_litt%C3%A9raires_2005)
- [2008](https://fr.wikipedia.org/wiki/2008_en_litt%C3%A9rature) : _Le Roman de la Bretagne_, éditions du Rocher

### Pamphlet

- [2017](https://fr.wikipedia.org/wiki/2017_en_litt%C3%A9rature) : _Du bonheur d'être breton. Les régions contre les nations_, éditions des Équateurs

### Divers

- 2015 : _Une mer de famille_, illustrations de [Nono](https://fr.wikipedia.org/wiki/Nono_(illustrateur)), [Éditions Dialogues](https://fr.wikipedia.org/wiki/Dialogues_(librairie))[\[4\]](#cite_note-:0-4)

## Notes et références

1. [↑](#cite_ref-1) Par décret en date du 27 novembre 1969, Gilles Martin, né le [18](https://fr.wikipedia.org/wiki/18_ao%C3%BBt) [août](https://fr.wikipedia.org/wiki/Ao%C3%BBt_1954) [1954](https://fr.wikipedia.org/wiki/1954) à Neuilly-sur-Seine, est autorisé à s'appeler Martin-Chauffier. 
 Source : [Emmanuel Ratier](https://fr.wikipedia.org/wiki/Emmanuel_Ratier), Encyclopédie des changements de noms, t. I \[1963-juin 1982], Paris, Faits et documents, 1995, 320 p. ([ISBN](https://fr.wikipedia.org/wiki/International_Standard_Book_Number) [2-909769-03-8](https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Ouvrages_de_r%C3%A9f%C3%A9rence/2-909769-03-8)), p. 222.
2. [↑](#cite_ref-2) [Historique du Mercure de France](https://www.mercuredefrance.fr/Historique), [Mercure de France](https://fr.wikipedia.org/wiki/Mercure_de_France), consulté le 26 novembre 2019.[\[source insuffisante\]](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:V%C3%A9rifiabilit%C3%A9)
3. [↑](#cite_ref-3) [_Pourpre_](http://www.gallimard.fr/Catalogue/MERCURE-DE-FRANCE/Litterature-generale/Pourpre), [éditions Gallimard](https://fr.wikipedia.org/wiki/%C3%89ditions_Gallimard), consulté le 26 novembre 2019.
4. ↑ [a](#cite_ref-:0_4-0) et [b](#cite_ref-:0_4-1) Louis Gildas, « Lettres bretonnes. Une mer de famille », _[Le Télégramme](https://fr.wikipedia.org/wiki/Le_T%C3%A9l%C3%A9gramme)_,‎ 17 janvier 2016 ([lire en ligne](https://www.letelegramme.fr/livres/une-mer-de-famille-17-01-2016-10922741.php), consulté le 21 novembre 2018)
5. [↑](#cite_ref-5) [Voir sur _Agence Bretagne Presse_.](https://abp.bzh/le-prix-breizh-attribue-au-nazairien-stephane-hoffmann-10365)
6. [↑](#cite_ref-6) [Interview de Gilles Martin-Chauffier](http://www.turquie-news.com/rubriques/editos-tribune-libre/39970-interview-de-gilles-martin.html), _turquie-news.com_.
7. [↑](#cite_ref-7) Gilles Martin-Chauffier, « L’Arménie en larmes », _[Paris Match](https://fr.wikipedia.org/wiki/Paris_Match)_,‎ 16 janvier 2012 ([lire en ligne](http://www.parismatch.com/Chroniques/LIVRESQUE/L-Armenie-en-larmes-223876)).
8. [↑](#cite_ref-8) [Entretien Gilles Martin-Chauffier à _28 minutes_-Arte](http://telescoop.tv/browse/944418/9/28-minutes.html), _telescoop.tv_, consulté le 26 novembre 2019.
9. [↑](#cite_ref-9) [Voir sur _armenews.com_.](http://www.armenews.com/article.php3?id_article=133351)
10. [↑](#cite_ref-10) [« Le dixième prix Jean-Freustié a été décerné à _Une affaire embarrassante_ »](https://www.humanite.fr/node/126643), sur [humanite.fr](https://fr.wikipedia.org/wiki/L%27Humanit%C3%A9), 15 mars 1996.