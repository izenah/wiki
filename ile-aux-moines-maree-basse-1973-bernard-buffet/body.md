# Île-aux-Moines, marée basse - 1973 - Bernard Buffet

::: figure
![](ile-aux-moines-maree-basse-1973-bernard-buffet/media/ile-aux-moines-bernard-buffet.jpg)
:::


> Île-aux-Moines, maree basse - Morbihan, 1973 Huile sur toile. Signée en haut à gauche et datée en haut à droite. Titrée au dos. Oil on canvas. Signed upper left and dated upper right. Titled on the reverse. H_89 cm L_130 cm Provenance: collection privée Nous remercions la galerie Maurice Garnier de nous avoir confirmé que cette oeuvre est bien enregistrée dans leurs archives...
>
> – [Pierre Bergé et associés](https://www.pba-auctions.com/en/lot/100501/11401229)
