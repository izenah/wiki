# Izenah template

::: content
:::

::: script
wikipediaId = sam.getRings(boka.getActivePage(), 'a-comme-identifiant-wikipedia')
:::

{{? it.wikipediaId.length > 0 }}
Le contenu ci-dessous est issu de [Wikipédia](https://fr.wikipedia.org/wiki/{{= it.wikipediaId[0].value.substring(3) }}) – © Wikipédia – Licence Creative Commons BY-SA

::: cat
reference: wikipedia.fr.md@
:::
{{?}}



