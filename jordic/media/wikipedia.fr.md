Georges Jordic-Pignon

[![Jordic - Marie aux sabot de bois se gage page1.jpeg](https://upload.wikimedia.org/wikipedia/commons/thumb/8/81/Jordic_-_Marie_aux_sabot_de_bois_se_gage_page1.jpeg/260px-Jordic_-_Marie_aux_sabot_de_bois_se_gage_page1.jpeg)](/wiki/Fichier:Jordic_-_Marie_aux_sabot_de_bois_se_gage_page1.jpeg)

Illustration de Marie aux sabots de bois se gage - page 1

| Naissance | [5](https://fr.wikipedia.org/wiki/5_janvier) [janvier](https://fr.wikipedia.org/wiki/Janvier_1876) [1876](https://fr.wikipedia.org/wiki/1876) [Skikda](https://fr.wikipedia.org/wiki/Skikda) |
| - | - |
| Décès | [28](https://fr.wikipedia.org/wiki/28_novembre) [novembre](https://fr.wikipedia.org/wiki/Novembre_1915) [1915](https://fr.wikipedia.org/wiki/1915) (à 39 ans) |
| Pseudonyme | Jordic |
| Nationalité | [Français](https://fr.wikipedia.org/wiki/France) |
| Activités | [Peintre](https://fr.wikipedia.org/wiki/Artiste_peintre), [illustrateur](https://fr.wikipedia.org/wiki/Illustrateur) |
| Conjointe | Marguerite Géniaux ([d](https://www.wikidata.org/wiki/Q83423639 "d:Q83423639")) (depuis [1899](https://fr.wikipedia.org/wiki/1899)) |
| Distinction | [Mort pour la France](https://fr.wikipedia.org/wiki/Mort_pour_la_France) |

 -  - [![Documentation du modèle](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Info_Simple.svg/12px-Info_Simple.svg.png)](https://fr.wikipedia.org/wiki/Mod%C3%A8le:Infobox_Artiste)

**Georges Jordic-Pignon** dit **Jordic** est un [illustrateur](https://fr.wikipedia.org/wiki/Illustrateur) et [peintre](https://fr.wikipedia.org/wiki/Artiste_peintre) [français](https://fr.wikipedia.org/wiki/France), né le [5](https://fr.wikipedia.org/wiki/5_janvier) [janvier](https://fr.wikipedia.org/wiki/Janvier_1876) [1876](https://fr.wikipedia.org/wiki/1876) à [Philippeville](https://fr.wikipedia.org/wiki/Skikda) ([Algérie](https://fr.wikipedia.org/wiki/Alg%C3%A9rie)) et tombé pour la [France](https://fr.wikipedia.org/wiki/France) le [28](https://fr.wikipedia.org/wiki/28_novembre) [novembre](https://fr.wikipedia.org/wiki/Novembre_1915) [1915](https://fr.wikipedia.org/wiki/1915)[\[1\]](#cite_note-1).



## Biographie

Élève des peintres [Benjamin-Constant](https://fr.wikipedia.org/wiki/Benjamin-Constant) et [Jean-Paul Laurens](https://fr.wikipedia.org/wiki/Jean-Paul_Laurens), Jordic est auteur illustrateur de livre pour enfants. Il a exposé au Salon de la Société des Artistes français entre 1895 à 1907. Il se marie le 29 juillet 1899 à Marguerite Géniaux, sœur de [Charles](https://fr.wikipedia.org/wiki/Charles_G%C3%A9niaux) et [Paul Géniaux](https://fr.wikipedia.org/wiki/Paul_G%C3%A9niaux)[\[2\]](#cite_note-:0-2). Ils ont une fille Simone née le 17 avril 1903[\[2\]](#cite_note-:0-2).

En 1915, il meurt pour la France[\[3\]](#cite_note-3), son nom ne figure néanmoins pas au Panthéon avec les [546 écrivains](https://fr.wikipedia.org/wiki/Liste_des_personnes_cit%C3%A9es_au_Panth%C3%A9on_de_Paris#Écrivains_morts_au_champ_d'honneur) morts au champ d'honneur.

## Œuvres

- Auteur-Illustrateur
- _Marie aux Sabots de bois se gage_, Garnier, n. d.[\[4\]](#cite_note-4).
- _Les Dernières Places de Marie aux Sabots de Bois_.
- _Perrine la petite laitière_, Garnier, n. d.
- _Les Petits Brazidec à Paris_, Garnier, n. d.
- _Lillette Léveillé à Craboville_.
- _Les Sept Jours de Ketje_, Garnier frères.
- _Cours Select Directrice Miss Bigoudy_, Librairie Garnier Frères, 1928.
- Illustration
- _La Dame aux Camélia_, d'[Alexandre Dumas fils](https://fr.wikipedia.org/wiki/Alexandre_Dumas_fils), Paris, Calmann-Levy, 1908.
- [Marcelle Tinayre](https://fr.wikipedia.org/wiki/Marcelle_Tinayre), [Hellé](https://fr.wikisource.org/wiki/fr:Hell%C3%A9 "s:fr:Hellé"), Paris, Calmann-Lévy, 1909 ([Wikisource](https://fr.wikipedia.org/wiki/Wikisource)).
- _Ceux qui espèrent de Jules Imbert_, Tours Mame.
- _Le jardin enchanté_, de Mme Tony d'Ulmès (pseudonyme de Berthe Rey) Lausanne, Librairie Payot, 1910.
- _L'Étoile du Pacifique_, de Georges Price, Mame, 1910, réédité par Banquises et Comètes, 2015.
- Peintures
- _La Tempête apaisée_, 1899, dans l'église de Billiers[\[5\]](#cite_note-5).

## Références

1. [↑](#cite_ref-1) (en) [« Georges Jordic-Pignon »](https://doi.org/10.1093/benz/9780199773787.article.B00095794), extrait de la notice dans le _dictionnaire [Bénézit](https://fr.wikipedia.org/wiki/B%C3%A9n%C3%A9zit)_, sur [Oxford Art Online](https://fr.wikipedia.org/wiki/Oxford_Art_Online), 2011 ([ISBN](https://fr.wikipedia.org/wiki/International_Standard_Book_Number) [9780199773787](https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Ouvrages_de_r%C3%A9f%C3%A9rence/9780199773787))
2. ↑ [a](#cite_ref-:0_2-0) et [b](#cite_ref-:0_2-1) Laurence Prod'homme, Nathalie Boulouch et Linda Garcia d'Ornano (préf. Céline Chanas), Charles et Paul Géniaux : la photographie, un destin, Châteaulin/Rennes, Locus solus / Musée de Bretagne, 191 p. ([ISBN](https://fr.wikipedia.org/wiki/International_Standard_Book_Number) [978-2-36833-266-5](https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Ouvrages_de_r%C3%A9f%C3%A9rence/978-2-36833-266-5), [OCLC](https://fr.wikipedia.org/wiki/Online_Computer_Library_Center) [1128867965](https://worldcat.org/oclc/1128867965&lang=fr), [lire en ligne](https://www.worldcat.org/oclc/1128867965)), p. 191
3. [↑](#cite_ref-3) papier qui mentionne les circonstances de la mort disponible sur le site [Mémoire des hommes](http://www.memoiredeshommes.sga.defense.gouv.fr/) : [facsimilé](http://www.memoiredeshommes.sga.defense.gouv.fr/SrvImg/SrvImg.php?_B=1&_I=XihsXdhWhAKydl+AumCEEQ==&_C=2666080546) où l'on peut lire : Pignon Georges, Louis sergent, 18e régiment d'infanterie, matricule 1629 au corps classe 1896, 172 au recrutement Vannes, mort pour la France le 28 novembre 1915 dans l'ambulance 5/170 à Houdoins (Pas-de-Calais) ; genre de mort : suite des blessures de guerre, ; né le 9 janvier 1876 à Philippeville Départemetn Constantine transcrit le 1er mars 1916 à l'Isle aux Moines Morbihan
4. [↑](#cite_ref-4) 1911 cf. _La Bretagne_ par François de Beaulieu, date l'ouvrage de 1911 [p42](https://books.google.com/books?id=qpfDj7o6_kgC&pg=PA42) ou Polybiblion: Revue bibliographique universelle - Page 559 de 1912 recense l'album. - ré-édité en 1927 [catalogue Opale de la bnf](http://catalogue.bnf.fr/ark:/12148/cb32289433w/PUBLIC).
5. [↑](#cite_ref-5) [Site de Billiers](http://www.billiers.fr/Culture/Patrimoine/default.aspx?Art=3103&qry=) et [la toile](http://www.billiers.fr/Diaporama.aspx?IDG=1508&IDP=3672&diaporama=undefined)

## Annexes

### Bibliographie

- Marcus Osterwalder (dir.), _Dictionnaire des illustrateurs, 1890-1945_, Éditions Ides et Calendes, 1992. p. 569.

- Martine de la Ferrière, _Jordic un artiste à l'Ile aux Moines avant 1914_

 Martine de la Ferrière est la petite fille de Georges Jordic-Pignon.

- Martine de la Ferrière, « Jordic », _[Le Collectionneur de bandes dessinées](https://fr.wikipedia.org/wiki/Le_Collectionneur_de_bandes_dessin%C3%A9es)_, no 107, printemps, 2006, p. 28-35.