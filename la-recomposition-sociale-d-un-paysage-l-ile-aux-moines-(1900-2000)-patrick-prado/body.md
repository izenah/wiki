# La recomposition sociale d'un paysage : l'île aux Moines (1900-2000) - Patrick Prado

Dans Ethnologie française 2006/3 (Vol. 36), pages 457 à 466
https://www.cairn.info/revue-ethnologie-francaise-2006-3-page-457.htm


>Les îles ont longtemps été des lieux d’expérimentation et de spéculation, des laboratoires idéologiques et politiques de nos sociétés. Il en a résulté le pire plutôt que le meilleur : utopies rigoristes, lieux clos, fantasmes du temps suspendu et de la perfection de l’État, crimes parfaits. Les îles sont « bonnes à penser » : des Grecs à la Renaissance, voire à nos jours, il s’agit le plus souvent d’îles à topos \[Prado, 2000b]. Elles rassemblent aussi d’une façon extrême les valeurs en cours d’une époque. L’île est un intensificateur du sentiment autant que de l’analyse. 
