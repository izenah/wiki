# Le légendaire vicaire Guillemet - Léone Morice - mai 1983

Source : Gazette 115, mai 1983

L'abbé GUILLEMET, vicaire à l'Ile-aux-Moines de 1833 à 1856, était un personnage hors du commun. Sans doute homme d'action tenace et bâtisseur infatigable, celui que Jean EVENOU dépeint dans son article sur les carrières de l'Ile-aux-Moines. Mais s'il est passé dans la légende et si l'on en parle toujours avec effroi près d'un siècle et demi plus tard, c'est pour un tout autre aspect de sa personnalité. Un fait à cet égard est assez éloquent : quand nos grands parents évoquaient son souvenir, quand les vieilles gens l'évoquent encore, ils ne l'appellent jamais, comme les autres prêtres qui se sont succédé sur l'île et qui furent tous aimés et infiniment respectés, Monsieur....... , ni l'abbé....... mais GUILLEMET, seulement. Ceci en dit long sur le peu d'affection que ses paroissiens lui témoignaient. À vrai dire, l'épithète, la seule épithète, qui revient toujours aux lèvres des Îlois à son souvenir, c'est : " Il était méchant. Oh qu'il était méchant ! ".

Dame, il avait lu le " Petit Albert " et probablement même le " Grand Albert " Il en avait le droit, étant prêtre, mais il en avait tiré une science dont il usait pour le salut des âmes.... peut-être ; pour assouvir sa méchanceté.... sans doute. À vrai dire, il passait pour magicien, voire pour sorcier. Jugez-en plutôt à ces histoires, naïves, c'est sûr, mais révélatrices de son caractère et des sentiments de la population à son endroit :

Deux sœurs se souvinrent longtemps de la frayeur qu'il leur fit un beau soir d'hiver. Toutes deux étaient descendues chez le vicaire. Il s'était en effet fait construire une maison, celle qui, dans son enclos sur la plage de Port-Miquel, appartient aujourd'hui à la famille WALTER. Totalement illettrées, chose courante à cette époque, elles venaient lui demander de leur écrire une lettre. Il accepta, mais fit durer le travail ( à dessein, dit-on ) jusqu'à la nuit, connaissant la peur que les deux femmes avaient du noir. Il proposa alors de les raccompagner, ce qu'elles acceptèrent avec gratitude. Ils gravirent l'escalier de pierre jusqu'au cimetière qui entourait encore l'église. Mais en passant devant l'ossuaire, il saisit un crâne qu'il planta dans la main de l'une d'elles en
lançant : " Tiens, voilà la tête de ton pépé ", Sur quoi il dégringola l'escalier en riant, laissant là les deux malheureuses hurlant d'épouvante.

Grosse farce ! Grosse farce seulement.....

Mais voici d'autres contes : on lui prêtait des pouvoirs magiques. Il jouait des animaux qu'il " possédait " absolument pour tourmenter les gens. C'est ainsi que si quiconque s'avisait de battre un chien ensorcelé par lui, c'était lui-même qui ressentait la douleur à la place de la bête.

On se raconte toujours cette histoire : il y avait souvent à l'entrée des venelles, une pierre dressée qu'il fallait enjamber ( un " posiann ", disions-nous ).

Elle était censée empêcher le passage des animaux. GUILLEMET, connaissant le parcours emprunté régulièrement par un douanier qui avait le malheur de lui déplaire, avait placé un chien ensorcelé par lui, au pied d'un de ces " posiann's ", espérant bien la suite: l'homme frapperait la bête pour se faire un passage et se trouverait rossé du même coup. Las! le douanier, instruit par l'expérience de plus d'un, éventa la ruse..... caressa le chien !

Quant à Gilette du Port-Blanc ( le pouvoir de GUILLEMET s'étendait jusque 1à, bien des gens de la Grande Terre venant entendre la messe sur l'île ) ce GUILLEMET donc, devant un jour se rendre à Auray, avait attelé son char à bancs. Il ne put jamais faire avancer son cheval devant lequel se dressait à chaque pas un rideau de flammes.

C'est encore notre vicaire qui, invité à un repas au Bindo, fit le pari de ressuciter le coq dressé sur le plat, aussi nu que l'enfant naissant, aussi mort que le corps-mort du bateau du maître de céans.

" Machoire ! s'écria-t-il, vous allez l'entendre, aussi vrai que vous m'entendez. Et vous allez le voir, aussi vivant que vous me voyez&nbsp;!&nbsp;".

" Machoire ! " était son juron favori. Il en émaillait ses propos. À vrai dire, il se moquait seulement, par là, des " dames " de l'Ile-aux-Moines qui se donnaient à plaisir du " ma chère " par ci, " ma chère “ par là et le prononçaient " ma chouère ! " à la mode du lieu et du temps. De " ma chouère " à " machoire " , n'est-ce pas?.....

Mais revenons donc à notre coq tout inerte dans son plat... Sur un geste du vicaire, il fut debout, plumes, queue et crête, planté hardi sur ses deux pattes, tonitruant un vigoureux cocorico..... Les convives de s'enfuir en toutes directions sans démander leur reste.....

Des bêtes, des bêtes encore, surgirent un jour, plus effrayantes les unes que les autres, sur le chemin de deux amoureux qui s'étaient donné rendez-vous à Brouël et qui ne purent jamais se rejoindre.

Car à vrai dire, il tourmentait surtout les amoureux. Il traquait ceux qui s'attardaient dans les chemins creux et surgissait devant eux, tel une apparition, enveloppé d'un drap blanc ou encore revêtu d'une peau de mouton, et toujours armé d'un solide “ breupiann " dont il allait jusqu'à 1es battre, dit-on. Sur quoi, il disparaissait, se dissolvait littéralement dans l'air, aussi mystérieusement qu'il leur était apparu.

Certes plus cruel encore : quand il payait ses ouvriers carriers du Trech, il jetait dans la vase les quelques pièces qui leur revenaient, pour avoir la joie, la joie horrible de les humilier.

Ce sont là quelques unes des histoires de sorcellerie qui se disent encore.....

   Léone MORICE

 

