# Le passage de l’Île aux Moines – Guillaume Moingeon

https://www.verdieditions.com/produit/passage-ile-moines-moingeon/

>Retrace l’histoire des passeurs bateliers qui effectuaient les passages réguliers depuis le continent jusqu’à l’Ile aux Moines dans le Golfe du Morbihan, à travers les témoignages de passeurs retraités ou en activité et d’usagers. Le passage entre l’île et le continent a longtemps été assuré à la demande par des pêcheurs ou des ostréiculteurs, avant de devenir une liaison régulière en 1920. © Electre 2020 – [Verdi Éditions](https://www.verdieditions.com/produit/passage-ile-moines-moingeon/)


::: image
reference: passage-ile-aux-moines-moingeon.jpg
caption: Crédits photo : Verdi Éditions
:::

