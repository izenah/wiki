| Naissance | [24](https://fr.wikipedia.org/wiki/24_ao%C3%BBt) [août](https://fr.wikipedia.org/wiki/Ao%C3%BBt_1894) [1894](https://fr.wikipedia.org/wiki/1894) [Vannes](https://fr.wikipedia.org/wiki/Vannes) |
| - | - |
| Décès | [6](https://fr.wikipedia.org/wiki/6_octobre) [octobre](https://fr.wikipedia.org/wiki/Octobre_1980) [1980](https://fr.wikipedia.org/wiki/1980) (à 86 ans) [Puteaux](https://fr.wikipedia.org/wiki/Puteaux) |
| Nom de naissance | Louis Marie Jean Martin |
| Nationalité | [Français](https://fr.wikipedia.org/wiki/France) |
| Formation | [École des chartes](https://fr.wikipedia.org/wiki/%C3%89cole_nationale_des_chartes) |
| Activités | [Journaliste](https://fr.wikipedia.org/wiki/Journaliste), [critique littéraire](https://fr.wikipedia.org/wiki/Critique_litt%C3%A9raire), [résistant](https://fr.wikipedia.org/wiki/R%C3%A9sistance_int%C3%A9rieure_fran%C3%A7aise), [bibliothécaire](https://fr.wikipedia.org/wiki/Biblioth%C3%A9caire) |
| Conjointe | Simone Martin-Chauffier ([d](https://www.wikidata.org/wiki/Q11774913 "d:Q11774913")) |

| A travaillé pour | [Le Parisien](https://fr.wikipedia.org/wiki/Le_Parisien), [Le Figaro](https://fr.wikipedia.org/wiki/Le_Figaro), [Paris Match](https://fr.wikipedia.org/wiki/Paris_Match) |
| - | - |
| Membre de | [Académie des sciences morales et politiques](https://fr.wikipedia.org/wiki/Acad%C3%A9mie_des_sciences_morales_et_politiques) |
| Distinctions | [Grand prix de littérature de la SGDL](https://fr.wikipedia.org/wiki/Grand_prix_de_litt%C3%A9rature_de_la_SGDL) [Commandeur de la Légion d'honneur‎](https://fr.wikipedia.org/wiki/Liste_des_commandeurs_de_la_L%C3%A9gion_d%27honneur) (1954) [Prix Breizh](https://fr.wikipedia.org/wiki/Prix_Breizh) (1962) |

 -  - [![Documentation du modèle](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Info_Simple.svg/12px-Info_Simple.svg.png)](https://fr.wikipedia.org/wiki/Mod%C3%A8le:Infobox_Biographie2)

**Louis Martin-Chauffier**, né **Louis Martin**[\[1\]](#cite_note-1), est un [journaliste](https://fr.wikipedia.org/wiki/Journaliste), [écrivain](https://fr.wikipedia.org/wiki/%C3%89crivain) et [résistant](https://fr.wikipedia.org/wiki/R%C3%A9sistance_fran%C3%A7aise) français né le [24](https://fr.wikipedia.org/wiki/24_ao%C3%BBt) [août](https://fr.wikipedia.org/wiki/Ao%C3%BBt_1894) [1894](https://fr.wikipedia.org/wiki/1894) à [Vannes](https://fr.wikipedia.org/wiki/Vannes) et mort le [6](https://fr.wikipedia.org/wiki/6_octobre) [octobre](https://fr.wikipedia.org/wiki/Octobre_1980) [1980](https://fr.wikipedia.org/wiki/1980) à [Puteaux](https://fr.wikipedia.org/wiki/Puteaux)[\[2\]](#cite_note-2).



## Biographie

- Formation

Louis Martin-Chauffier commence des études de médecine puis, après la mort de son père[\[3\]](#cite_note-ji-3), passe le concours de l’[École nationale des chartes](https://fr.wikipedia.org/wiki/%C3%89cole_nationale_des_chartes), où il est reçu en 1915. Pendant la [Première Guerre mondiale](https://fr.wikipedia.org/wiki/Premi%C3%A8re_Guerre_mondiale), il est cependant mobilisé comme médecin auxiliaire. Il reprend sa scolarité en 1919 et devient [archiviste paléographe](https://fr.wikipedia.org/wiki/Archiviste_pal%C3%A9ographe) en 1921, année de son mariage avec Simone Duval[\[4\]](#cite_note-nr-4). Il est alors nommé bibliothécaire à la [bibliothèque Mazarine](https://fr.wikipedia.org/wiki/Biblioth%C3%A8que_Mazarine), puis à [Florence](https://fr.wikipedia.org/wiki/Florence) (1923-1927)[\[3\]](#cite_note-ji-3).

- L'entre-deux-guerres

Dès 1922, il fait paraître son premier roman, _La Fissure_. Durant les années 1920, Louis Martin-Chauffier écrit quatre romans avant d’abandonner ce genre auquel il ne reviendra qu’en 1950.

Il collabore aussi à la maison d’édition [Au sans pareil](https://fr.wikipedia.org/wiki/Au_sans_pareil), où il publie des auteurs d'avant-garde comme [Blaise Cendrars](https://fr.wikipedia.org/wiki/Blaise_Cendrars), rédigeant une présentation de [Philippe Soupault](https://fr.wikipedia.org/wiki/Philippe_Soupault) en annexe d’_Histoire d'un Blanc_, ou signant la préface d’_Aspects de la biographie_ d’[André Maurois](https://fr.wikipedia.org/wiki/Andr%C3%A9_Maurois). Il effectue également des traductions de classiques ([Aristophane](https://fr.wikipedia.org/wiki/Aristophane), [Dante](https://fr.wikipedia.org/wiki/Dante), etc.) pour des éditions illustrées de luxe ; dans les années 1930, il entreprend la première édition des œuvres complètes d'[André Gide](https://fr.wikipedia.org/wiki/Andr%C3%A9_Gide) (1932-1939), et travaille pendant plus de quinze ans à une étude de [Chateaubriand](https://fr.wikipedia.org/wiki/Fran%C3%A7ois-Ren%C3%A9_de_Chateaubriand), publiée en 1943 sous le titre _Chateaubriand ou l'obsession de la pureté_. Il s'occupe de [Luce Vigo](https://fr.wikipedia.org/wiki/Luce_Vigo), fille du cinéaste [Jean Vigo](https://fr.wikipedia.org/wiki/Jean_Vigo) et de sa femme morts prématurément.

Il a aussi une activité de journaliste : tout en étant bibliothécaire, il donne des articles à diverses revues, en particulier à la _[Revue critique des idées et des livres](https://fr.wikipedia.org/wiki/Revue_critique_des_id%C3%A9es_et_des_livres)_, proche de [l'Action française](https://fr.wikipedia.org/wiki/Action_fran%C3%A7aise), puis devient chroniqueur religieux au _[Figaro](https://fr.wikipedia.org/wiki/Figaro)_.

Par la suite, il est rédacteur en chef de divers hebdomadaires, orientés nettement à gauche, comme _[Lu](/w/index.php?title=Lu_(magazine)&action=edit&redlink=1 "Lu (magazine) (page inexistante)")_, _[Vu](https://fr.wikipedia.org/wiki/Vu_(magazine))_, puis _[Vendredi](https://fr.wikipedia.org/wiki/Vendredi_(hebdomadaire))_. En 1938, il devient directeur littéraire de _[Match](https://fr.wikipedia.org/wiki/Paris_Match)_ et éditorialiste à _[Paris-Soir](https://fr.wikipedia.org/wiki/Paris-Soir)_.

- La Seconde Guerre mondiale

En [1940](https://fr.wikipedia.org/wiki/1940), il part en [zone libre](https://fr.wikipedia.org/wiki/Zone_libre) avec l’équipe de son journal. Il entre dans la [Résistance](https://fr.wikipedia.org/wiki/R%C3%A9sistance_int%C3%A9rieure_fran%C3%A7aise), devenant rédacteur en chef d’un des plus importants journaux clandestins, [_Libération_](https://fr.wikipedia.org/wiki/Lib%C3%A9ration_(journal,_1941-1964)) en 1942. En avril 1944, il est arrêté par la [Gestapo](https://fr.wikipedia.org/wiki/Gestapo) et envoyé en camp de concentration, d’abord à [Neuengamme](https://fr.wikipedia.org/wiki/Neuengamme) puis à [Bergen-Belsen](https://fr.wikipedia.org/wiki/Bergen-Belsen)[\[5\]](#cite_note-5). À la [Libération](https://fr.wikipedia.org/wiki/Lib%C3%A9ration_de_la_France), il est délégué à l'[Assemblée consultative provisoire](https://fr.wikipedia.org/wiki/Assembl%C3%A9e_consultative_provisoire) (juillet-août 1945) au titre des prisonniers et déportés, puis il poursuit sa carrière de journaliste et continue à faire vivre le journal sorti de la clandestinité : il est directeur littéraire de [_Libération_](https://fr.wikipedia.org/wiki/Lib%C3%A9ration_(journal,_1941-1964)), le quotidien dirigé par [Emmanuel d'Astier de la Vigerie](https://fr.wikipedia.org/wiki/Emmanuel_d%27Astier_de_la_Vigerie).

- L'après-guerre

Il travaille ensuite pour divers quotidiens et hebdomadaires : chef du service étranger du _[Parisien libéré](https://fr.wikipedia.org/wiki/Le_Parisien)_, chroniqueur littéraire à _[Paris-Presse](https://fr.wikipedia.org/wiki/Paris-Presse)_ et à _[Paris Match](https://fr.wikipedia.org/wiki/Paris_Match)_, directeur de la rédaction de _Fémina-Illustration_.

Son œuvre de romancier et son action en faveur de la littérature contemporaine ne lui font pas oublier les grands classiques : il est l'éditeur des œuvres complètes de [La Rochefoucauld](https://fr.wikipedia.org/wiki/Fran%C3%A7ois_de_La_Rochefoucauld) dans la [bibliothèque de la Pléiade](https://fr.wikipedia.org/wiki/Biblioth%C3%A8que_de_la_Pl%C3%A9iade).

Il s'engage aussi en tant qu'ancien résistant et déporté et, dans les années 1950, est une des cibles des attaques (verbales) des négationnistes ou révisionnistes de l'époque ([Paul Rassinier](https://fr.wikipedia.org/wiki/Paul_Rassinier), [Albert Paraz](https://fr.wikipedia.org/wiki/Albert_Paraz), [Maurice Bardèche](https://fr.wikipedia.org/wiki/Maurice_Bard%C3%A8che)). En 1952, il intervient dans _Le Figaro littéraire_ pour répondre au pamphlet de [Jean Paulhan](https://fr.wikipedia.org/wiki/Jean_Paulhan) _Lettre aux directeurs de la Résistance_[\[6\]](#cite_note-6). En 1967, il écrit dans sa préface à l'ouvrage collectif _la déportation_[\[7\]](#cite_note-7), qu'il faut lutter, sans haine ni violence, contre l’oubli du passé[\[4\]](#cite_note-nr-4).

Durant la [guerre d'Algérie](https://fr.wikipedia.org/wiki/Guerre_d%27Alg%C3%A9rie), il participe activement à une « Commission internationale sur le système concentrationnaire », qui, en 1957 (époque de la [bataille d'Alger](https://fr.wikipedia.org/wiki/Bataille_d%27Alger)), mène sur place une enquête sur le système répressif établi par l'armée française[\[8\]](#cite_note-8).

Il est le grand-père du journaliste [Gilles Martin-Chauffier](https://fr.wikipedia.org/wiki/Gilles_Martin-Chauffier).[\[réf. souhaitée\]](https://fr.wikipedia.org/wiki/Aide:R%C3%A9f%C3%A9rence_n%C3%A9cessaire)

## Distinctions

- [1947](https://fr.wikipedia.org/wiki/1947) : [grand prix de littérature de la SGDL](https://fr.wikipedia.org/wiki/Grand_prix_de_litt%C3%A9rature_de_la_SGDL) pour l’ensemble de son œuvre.
- [1957](https://fr.wikipedia.org/wiki/1957) : [grand prix national des Lettres](https://fr.wikipedia.org/wiki/Grand_prix_national_des_Lettres) (ministère de la Culture) pour sa contribution au rayonnement des lettres françaises.
- [1962](https://fr.wikipedia.org/wiki/1962) : [prix Breizh](https://fr.wikipedia.org/wiki/Prix_Breizh) pour l’ensemble de son œuvre.

Il est élu membre de l’[Académie des sciences morales et politiques](https://fr.wikipedia.org/wiki/Acad%C3%A9mie_des_sciences_morales_et_politiques) en [1964](https://fr.wikipedia.org/wiki/1964).

## Hommage

Une salle de réunion de l'Institut de France à Paris porte son nom.

## Principales œuvres

- _L’Affaire des évêques simoniaques bretons et l’érection de Dol en métropole (848-850)_, thèse de l’École des chartes, 1921.
- _Correspondances apocryphes, Mme de Vandeul et Diderot, Choderlos de Laclos, Flaubert, Barbey d'Aurevilly, Marcel Proust, Anatole France..._, 1923 (préface de [Pierre Benoit](https://fr.wikipedia.org/wiki/Pierre_Benoit))
- _La Fissure_, roman, 1923
- _Patrice, ou l’indifférent_, roman, 1924
- _L’Épervier_, roman, 1925
- _L’Amant des honnêtes femmes_, roman, 1927
- _Jeux de l’âme_, roman, 1927
- _La Paix_ d’Aristophane, traduction, 1930
- _L’Enfer_ de Dante, traduction, 1930
- Œuvres complètes d’[André Gide](https://fr.wikipedia.org/wiki/Andr%C3%A9_Gide), édition, 1932-1939
- _Chateaubriand ou l’obsession de la pureté_, essai, 1943
- _L’homme et la bête_, essai, 1947
- _Mon père n’est pas mort_, roman, 1950
- _L’Écrivain et la liberté_, essai, 1958
- Œuvres complètes de [La Rochefoucauld](https://fr.wikipedia.org/wiki/Fran%C3%A7ois_de_La_Rochefoucauld), édition, 1964
- _Chroniques d’un homme libre_, 1989

## Annexes

### Bibliographie

- Jean Imbert, « Louis Martin-Chauffier », dans _[Bibliothèque de l'École des chartes](https://fr.wikipedia.org/wiki/Biblioth%C3%A8que_de_l%27%C3%89cole_des_chartes)_, 1982, no 140-142, p. 353-354