| - | - |
| Petites Sœurs de saint François d'Assise | |
| [![Image illustrative de l’article Petites Sœurs de saint François d'Assise](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Kruis_san_damiano_bright.gif/100px-Kruis_san_damiano_bright.gif)](https://fr.wikipedia.org/wiki/Fichier:Kruis_san_damiano_bright.gif) [Crucifix de Saint-Damien](https://fr.wikipedia.org/wiki/Crucifix_de_Saint-Damien), symbole de la congrégation | |
| Ordre de droit pontifical | |
| Approbation diocésaine | [8 décembre](https://fr.wikipedia.org/wiki/8_d%C3%A9cembre) [1873](https://fr.wikipedia.org/wiki/1873) par Mgr [Freppel](https://fr.wikipedia.org/wiki/Charles-%C3%89mile_Freppel) |
| Approbation pontificale | [15 janvier](https://fr.wikipedia.org/wiki/15_janvier) [1955](https://fr.wikipedia.org/wiki/1955) par [Pie XII](https://fr.wikipedia.org/wiki/Pie_XII) |
| Institut | [congrégation religieuse](https://fr.wikipedia.org/wiki/Congr%C3%A9gation_religieuse) |
| Type | apostolique |
| Spiritualité | franciscaine |
| Règle | [de saint François](https://fr.wikipedia.org/wiki/R%C3%A8gle_de_saint_Fran%C3%A7ois) |
| But | soins des malades |
| Structure et histoire | |
| Fondation | [8 décembre](https://fr.wikipedia.org/wiki/8_d%C3%A9cembre) [1873](https://fr.wikipedia.org/wiki/1873) [Angers](https://fr.wikipedia.org/wiki/Angers) |
| Fondateur | Joséphine Renaud |
| Abréviation | P.S.S.F |
| Patron | saint [François d'Assise](https://fr.wikipedia.org/wiki/Fran%C3%A7ois_d%27Assise) |
| Rattaché à | [frères mineurs capucins](https://fr.wikipedia.org/wiki/Fr%C3%A8res_mineurs_capucins) |
| Site web | [site officiel](http://petites.soeursfranciscaines.org/) |
| [Liste des ordres religieux](https://fr.wikipedia.org/wiki/Liste_d%27ordres_religieux_catholiques) | |
| | |

Les **Petites Sœurs de saint François d'Assise** forment une [congrégation religieuse](https://fr.wikipedia.org/wiki/Congr%C3%A9gation_religieuse) féminine enseignante et hospitalière de [droit pontifical](https://fr.wikipedia.org/wiki/Institut_religieux_de_droit_pontifical).



## Historique

En [1869](https://fr.wikipedia.org/wiki/1869), Louise Renaud en religion Mère Joséphine et trois compagnes, toutes membres du [Tiers-Ordre franciscain](https://fr.wikipedia.org/wiki/Tiers-Ordre_franciscain) se mettent en communauté sur l'avis et les conseils du Père Louis, religieux [capucin](https://fr.wikipedia.org/wiki/Fr%C3%A8res_mineurs_capucins), pour se consacrer aux soins des malades à domicile. Le [8 décembre](https://fr.wikipedia.org/wiki/8_d%C3%A9cembre) [1873](https://fr.wikipedia.org/wiki/1873), Mgr [Freppel](https://fr.wikipedia.org/wiki/Charles-%C3%89mile_Freppel), [évêque d'Angers](https://fr.wikipedia.org/wiki/Liste_des_%C3%A9v%C3%AAques_d%27Angers) approuve l'institut et douze sœurs font leur [profession religieuse](https://fr.wikipedia.org/wiki/Profession_religieuse)[\[1\]](#cite_note-1). Par décret de l'évêque, les communautés du tiers-ordre de saint François de [Cholet](https://fr.wikipedia.org/wiki/Cholet) et [Saumur](https://fr.wikipedia.org/wiki/Saumur) fondées quelques années auparavant par le père Hortode, curé de Cholet, sont réunies aux Petites Sœurs de saint François d'Assise[\[2\]](#cite_note-Laissac-2).

L'institut est agrégé à l'ordre des [Frères mineurs capucins](https://fr.wikipedia.org/wiki/Fr%C3%A8res_mineurs_capucins) le [25 avril](https://fr.wikipedia.org/wiki/25_avril) [1934](https://fr.wikipedia.org/wiki/1934) et reçoit du Pape le [décret de louange](https://fr.wikipedia.org/wiki/Decretum_laudis) le 28 février [1944](https://fr.wikipedia.org/wiki/1944), ses [constitutions](https://fr.wikipedia.org/wiki/Constitutions_religieuses) sont définitivement approuvées le 15 janvier [1955](https://fr.wikipedia.org/wiki/1955)[\[3\]](#cite_note-3).

## Fusion

Quatre congrégations ont fusionné avec les Petites Sœurs de saint François d’Assise :

- [1947](https://fr.wikipedia.org/wiki/1947) : Franciscaines du Sacré-Cœur de Saint-Quentin fondées à [Parpeville](https://fr.wikipedia.org/wiki/Parpeville) le [8 septembre](https://fr.wikipedia.org/wiki/8_septembre) [1867](https://fr.wikipedia.org/wiki/1867) par Mère Saint Augustin Jumaux et le père Virgile Adam. Les religieuses se dédiaient aux œuvres paroissiales, aux écoles, aux orphelins, et à la visite et soins des malades. Les constitutions sont approuvées par Mgr [Dours](https://fr.wikipedia.org/wiki/Jean_Dours) le 18 février [1876](https://fr.wikipedia.org/wiki/1876)[\[2\]](#cite_note-Laissac-2).
- [1951](https://fr.wikipedia.org/wiki/1951) : Sœurs de Saint-François de Douai dites religieuses de Sainte-Marie de Douai. Congrégation fondée par Monsieur Laforest de Lawarche pour s'occuper d'un hôpital et visiter les malades à domicile, l'institut reçoit l'approbation épiscopale de Mgr [Belmas](https://fr.wikipedia.org/wiki/Louis_Belmas) en 1825.
- [1964](https://fr.wikipedia.org/wiki/1964) : Pénitentes [récolettines](https://fr.wikipedia.org/wiki/Fr%C3%A8res_mineurs_r%C3%A9collets) de [Merville](https://fr.wikipedia.org/wiki/Merville_(Haute-Garonne)).
- [1968](https://fr.wikipedia.org/wiki/1968) : Petites Sœurs franciscaines de Notre-Dame de Lenne. Congrégation fondée en 1931 par Euphrasie Delhon (1887-1971) en religion Mère Marie-Madeleine de la Croix[\[4\]](#cite_note-4).

## Activités et diffusion

Les sœurs se dédient aux soins des malades, à l'enseignement par des cours d'[alphabétisation](https://fr.wikipedia.org/wiki/Alphab%C3%A9tisation) et des écoles, aux visites des prisonniers.

Elles sont présentes en France, [Algérie](https://fr.wikipedia.org/wiki/Alg%C3%A9rie) et [République centrafricaine](https://fr.wikipedia.org/wiki/R%C3%A9publique_centrafricaine).

La [maison-mère](https://fr.wikipedia.org/wiki/Maison-m%C3%A8re) est à [Angers](https://fr.wikipedia.org/wiki/Angers).

En 2017, la congrégation comptait 115 sœurs dans 20 maisons[\[5\]](#cite_note-5).

## Notes et références

1. [↑](#cite_ref-1) [« d'où-venons-nous »](http://psfrancoisdassise.catholique.fr/#/dou-venons-nous/4161734), sur <http://psfrancoisdassise.catholique.fr> (consulté le 15 juin 2016)
2. ↑ [a](#cite_ref-Laissac_2-0) et [b](#cite_ref-Laissac_2-1) Norbert de Laissac, Les Religieuses Franciscaines : notices, Paris, Poussielgue, 1897, 478 p. ([lire en ligne](https://ia902609.us.archive.org/9/items/lesreligieusesf00laisgoog/lesreligieusesf00laisgoog.pdf))
3. [↑](#cite_ref-3) (it) Guerrino Pelliccia e Giancarlo Rocca, Dizionario degli Istituti di Perfezione, Milan, Edizioni paoline, 2010
4. [↑](#cite_ref-4) [« Franciscaines de Notre-Dame de Lenne »](http://www.congregation.fr/home/aveyron-diocse-de-rodez/petites-soeurs-franciscaines-de-notre-dame-de-lenne), sur <http://www.congregation.fr> (consulté le 15 juin 2016)
5. [↑](#cite_ref-5) (it) [Annuaire pontifical](https://fr.wikipedia.org/wiki/Annuaire_pontifical), [Vatican](https://fr.wikipedia.org/wiki/Vatican), [Librairie éditrice vaticane](https://fr.wikipedia.org/wiki/Librairie_%C3%A9ditrice_vaticane), 2017 ([ISBN](https://fr.wikipedia.org/wiki/International_Standard_Book_Number) [978-88-209-9975-9](https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Ouvrages_de_r%C3%A9f%C3%A9rence/978-88-209-9975-9)), p. 1624

- [![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/IHS-monogram-Jesus-medievalesque.svg/28px-IHS-monogram-Jesus-medievalesque.svg.png)](https://fr.wikipedia.org/wiki/Portail:Catholicisme) [Portail du catholicisme](https://fr.wikipedia.org/wiki/Portail:Catholicisme)